/* SPDX-License-Identifier: GPL-2.0 */
/*
 * This is the online Runtime Verification (RV) interface.
 *
 * RV is a lightweight (yet rigorous) method that complements classical
 * exhaustive verification techniques (such as model checking and
 * theorem proving) with a more practical approach for complex systems,
 * where the development of a complete fine-grained model is not viable
 * due to human and computational resources.
 *
 * Instead of relying on a fine-grained model of a system (e.g., at the
 * instruction level), RV works by analyzing the trace of the system's
 * actual execution, comparing it against a formal specification of
 * the system behavior, generally at a more coarse-grained level.
 *
 * If, on the one hand, RV is limited by the execution coverage, on the
 * other hand, RV can give precise information on the runtime behavior
 * of the monitored system while enabling the reaction for unexpected
 * events, avoiding, for example, the propagation of a failure on
 * safety-critical systems.
 *
 * The development of this interface roots in the development of the
 * paper:
 *
 * DE OLIVEIRA, Daniel Bristot; CUCINOTTA, Tommaso; DE OLIVEIRA, Rômulo
 * Silva. Efficient formal verification for the Linux kernel. In:
 * International Conference on Software Engineering and Formal Methods.
 * Springer, Cham, 2019. p. 315-332.
 *
 * And:
 *
 * DE OLIVEIRA, Daniel Bristot, et al. Automata-based formal analysis
 * and verification of the real-time Linux kernel. PhD Thesis, 2020.
 *
 * Copyright (C) 2019-2021 Daniel Bristot de Oliveira <bristot@redhat.com>
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kobject.h>

#define MODULE_NAME "rv"

#include "rv.h"

DEFINE_MUTEX(interface_lock);
struct rv_interface rv_root;

struct dentry *get_monitors_root(void)
{
	return rv_root.monitors_dir;
}

int __init rv_init_interface(void)
{
	int retval = 0;

#ifdef	USE_TRACE_FS
	retval = tracing_init_dentry();
	if (retval)
		return -ENOMEM;
#endif
	mutex_lock(&interface_lock);

	rv_root.root_dir = rv_create_dir("rv", NULL);
	if (!rv_root.root_dir) {
		retval = -ENOMEM;
		goto ret_unlock;
	}

	rv_root.monitors_dir = rv_create_dir("monitors", rv_root.root_dir);

	if (!rv_root.monitors_dir) {
		retval = -ENOMEM;
		goto ret_remove;
	}

	if (init_rv_monitors(rv_root.root_dir))
		goto ret_remove;

	if (init_rv_reactors(rv_root.root_dir))
		goto ret_remove;

	monitoring_on=true;
	reacting_on=true;

	mutex_unlock(&interface_lock);

	return retval;

ret_remove:
	rv_remove(rv_root.root_dir);
ret_unlock:
	mutex_unlock(&interface_lock);
	return retval;
}

#ifdef MODULE
void rv_destroy_interface(void)
{
	mutex_lock(&interface_lock);
	rv_remove(rv_root.root_dir);
	mutex_unlock(&interface_lock);
}

module_init(rv_init_interface);
module_exit(rv_destroy_interface);

#else /* MODULE */
late_initcall(rv_init_interface);
#endif

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Daniel Bristot de Oliveira");
MODULE_DESCRIPTION("The Runtime Verification (RV) interface");

#include <linux/mutex.h>

struct rv_interface {
	struct dentry *root_dir;
	struct dentry *monitors_dir;
};

/*
 * If it is a module, use debugfs, otherwise this is
 * inside the kernel and will be placed in the tracefs.
 */
#ifdef MODULE
  #include <linux/debugfs.h>
  #include <linux/rv.h>
  #define rv_create_dir		debugfs_create_dir
  #define rv_create_file	debugfs_create_file
  #define rv_remove		debugfs_remove
#else /* MODULE */
  #include "trace.h"
  #include <linux/tracefs.h>
  #include <linux/rv.h>

  #define USE_TRACE_FS

  #define rv_create_dir		tracefs_create_dir
  #define rv_create_file	tracefs_create_file
  #define rv_remove		tracefs_remove
#endif


#define MAX_RV_MONITOR_NAME_SIZE	100
#define MAX_RV_REACTOR_NAME_SIZE	100

extern struct mutex interface_lock;

struct rv_reactor_def {
	struct list_head list;
	struct rv_reactor *reactor;
	/* protected by the monitor interface lock */
	int counter;
};

struct rv_monitor_def {
	struct list_head list;
	struct rv_monitor *monitor;
	struct rv_reactor_def *rdef;
	struct dentry *root_d;
	bool enabled;
	bool reacting;
};

extern bool monitoring_on;
struct dentry *get_monitors_root(void);
void reset_all_monitors(void);
int init_rv_monitors(struct dentry *root_dir);

extern bool reacting_on;
int reactor_create_monitor_files(struct rv_monitor_def *mdef);
int init_rv_reactors(struct dentry *root_dir);

#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-only
#
# dot2k: transform dot files into a monitor for the Linux kernel.
#
# For more information, see:
#   https://bristot.me/efficient-formal-verification-for-the-linux-kernel/
#
# Copyright 2018-2020 Red Hat, Inc.
#
# Author:
#  Daniel Bristot de Oliveira <bristot@redhat.com>

if __name__ == '__main__':
    from dot2.dot2k import dot2k
    import argparse
    import ntpath
    import os
    import platform
    import sys
    import sys
    import argparse

    parser = argparse.ArgumentParser(description='transform .dot file into kernel rv monitor')
    parser.add_argument('-d', "--dot", dest="dot_file", required=True)
    parser.add_argument('-t', "--monitor_type", dest="monitor_type", required=True)
    parser.add_argument('-n', "--model_name", dest="model_name", required=False)
    parser.add_argument("-D", "--description", dest="description", required=False)
    params = parser.parse_args()

    print("Opening and parsing the dot file %s" % params.dot_file)
    try:
        monitor=dot2k(params.dot_file, params.monitor_type)
    except Exception as e:
        print('Error: '+ str(e))
        print("Sorry : :-(")
        sys.exit(1)

    # easier than using argparse action.
    if params.model_name != None:
        print(params.model_name)

    print("Writing the monitor into the directory %s" % monitor.name)
    monitor.print_files()
    print("Done, now edit the %s/%s.c to add the instrumentation" % (monitor.name, monitor.name))
    print("Then, move the monitor folder to the KERNEL_DIR/tools/rv/monitors")
    print("and add the following line to KERNEL_DIR/tools/rv/Makefile")
    print("        obj-m += rv/monitors/%s/%s.o" % (monitor.name, monitor.name))
    print("and we are done!")

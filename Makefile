.PHONY: all
all:
	make -C rv
	make -C dot2

.PHONY: clean
clean:
	make -C rv clean
	make -C dot2 clean

.PHONY: install
install:
	make -C rv install
	make -C dot2 install

.PHONY: unload
unload:
	@-rmmod wwnr 2> /dev/null || true
	@-rmmod wip 2> /dev/null || true 
	@-rmmod rv_printk 2> /dev/null || true
	@-rmmod rv 2> /dev/null || true 

.PHONY: load
load:
	modprobe rv
	modprobe wwnr
	modprobe wip
	modprobe rv_printk

.PHONY: reload
reload: unload load
